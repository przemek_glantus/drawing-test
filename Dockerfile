FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
ADD . .
RUN dotnet build drawing_test.csproj -c Release -o /app
RUN dotnet publish drawing_test.csproj -c Release -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
RUN apt-get update && apt-get install -y --allow-unauthenticated libgdiplus libc6-dev libx11-dev libharfbuzz-dev
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "drawing_test.dll"]
